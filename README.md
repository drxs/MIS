## MIS
 本科毕业设计：教务管理系统模型，基于ASP.NET、C#语言、HTML、Microsoft Visual Studio Community 2019、SQL Server on linux 2017完成

## 功能

登录时，读取用户信息，并记录当前用户所属类型，根据不同用户类型，决定导航栏显示的具体项目。

- 学生：个人信息管理、开课信息查询、查询成绩信息、查询公告、收件箱、发件箱、发送私信、说明文档

- 教师：个人信息管理、添加新用户、开课信息查询、添加课程信息、当前讲授课程、所有成绩信息、添加成绩信息、查询公告、发布公告、收件箱、发件箱、发送私信、说明文档

- 管理员：个人信息管理、添加新用户、全部用户信息、开课信息查询、添加课程信息、所有成绩信息、添加成绩信息、查询公告、发布公告、收件箱、发件箱、发送私信、说明文档

## 演示视频
[点击跳转查看](https://pan.wangyusong.cn/Viewer/Video?single=true&shareKey=atqis6xw&path=/%E6%95%99%E5%8A%A1%E7%AE%A1%E7%90%86%E7%B3%BB%E7%BB%9F%E6%BC%94%E7%A4%BA%E8%A7%86%E9%A2%91.mp4 "点击跳转查看")

## 截图
>用户登录页面

![用户登录页面](https://file.wangyusong.cn/MIS/login.png_drxs)

>个人信息页面

![个人信息界面](https://file.wangyusong.cn/MIS/selfinfo.png_drxs)

>添加成绩页面

![添加成绩页面](https://file.wangyusong.cn/MIS/addscore.png_drxs)

>查询公告

![查询公告](https://file.wangyusong.cn/MIS/allnotice.png_drxs)

>发布公告

![发布公告](https://file.wangyusong.cn/MIS/addnotice.png_drxs)

>帮助文档

![帮助文档](https://file.wangyusong.cn/MIS/docs.png_drxs)

## 使用方法

1. 微软官方下载、安装Visual Studio 2019、SQL Server 2017及以上版本、SQL Server Management Studio最新版本；
2. 下载源代码>>[[点击下载]](https://github.com/drxs/MIS/archive/master.zip "[点击下载]")；
3. 下载数据库文件MIS.mdf与MIS.ld）>>[[点击下载(密码：drxs)]](https://pan.wangyusong.cn/s/dhzu6lx7)；
4. 将下载好的数据库文件“MIS”附加到SQL Server；
5. 使用VS打开项目，修改Web.config中数据库连接字符串ConnectionString及MISConnectionString；
6. 调试、运行。

## 更新记录

**2020.05.08**
1.修复：发送私信功能逻辑错误
2.优化：部分文字提示

**2020.04.08**
1.优化：所有表格样式、抽屉导航栏显示效果
2.优化：“个人信息”页面显示内容、“站内信”页面显示效果、“添加新用户”页面布局
3.优化：用户角色权限控制
4.优化：部分时间、ID数据由系统自动生成
5.新增：3天内自动登录功能
6.新增：开启https访问
7.修改：部分跳转判断逻辑、跳转提示
8.修复：System.TypeLoadException错误

**2020.03.22**
1.添加：站内信(私信)功能
2.添加：帮助文档
3.优化：界面微调
4.修复：登录界面背景显示错误问题

**2020.02.28**
1.优化：删除部分代码
2.代码：上传至Github

**2020.02.25**
1.部署：通过Jexus部署至Linux服务器

**2020.02.11**
1.更新：系统主体构建完成

## 代码参考
1. ASP.NET 4.5网站开发与应用--清华大学出版社
2. ASP.NET项目开发全程实录--清华大学出版社
3. C#技术与应用开发--清华大学出版社
4. [MDUI开发文档](https://www.mdui.org/docs/ "MDUI开发文档")
5. [Microsoft SQL 文档](https://docs.microsoft.com/zh-cn/sql/?view=sql-server-ver15 "Microsoft SQL 文档")
6. [Visual Studio 文档](https://docs.microsoft.com/zh-cn/sql/linux/sql-server-linux-overview?view=sql-server-linux-2017 "Visual Studio 文档")
7. [ASP.NET文档](https://docs.microsoft.com/zh-cn/aspnet/overview "ASP.NET文档")

**更新于 2020-04-08**
