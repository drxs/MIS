﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SendMessage.aspx.cs" Inherits="project.SendMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>发送私信--教务管理系统</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">发送私信</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <asp:Label ID="Label1" runat="server" Text="loading" Font-Size="17px"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label2" runat="server" Text="loading" Font-Size="17px"></asp:Label>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label3" runat="server" Text="接收者" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Name" CssClass="mdui-select" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:MISConnectionString %>' SelectCommand="SELECT [Name] FROM [用户信息表] ORDER BY [Type] DESC, [Id]"></asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" hidden="hidden">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label4" runat="server" Text="接收者ID" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="SqlDataSource2" DataTextField="Id" DataValueField="Id" CssClass="mdui-select" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:MISConnectionString %>' SelectCommand="SELECT [Id] FROM [用户信息表] WHERE ([Name] = @Name)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" PropertyName="SelectedValue" Name="Name" Type="String"></asp:ControlParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label5" runat="server" Text="私信内容(300字符)" CssClass="mdui-textfield-label"></asp:Label>
                <textarea class="mdui-textfield-input" runat="server" id="add_content"></textarea>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label6" runat="server" Text="发送时间" CssClass="mdui-textfield-label"></asp:Label>
                <asp:TextBox ID="add_time" runat="server" CssClass="mdui-textfield-input" Enabled="False"></asp:TextBox>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-3 mdui-center mdui-text-center">
                <asp:Button ID="Button1" runat="server" Text="发送" OnClick="Send_Btn" CssClass="mdui-btn mdui-btn-block mdui-shadow-9 mdui-btn-raised mdui-ripple mdui-color-light-blue mdui-text-color-white" />
            </div>
        </div>
        <br />
    </div>
</asp:Content>