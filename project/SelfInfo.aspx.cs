﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace project
{
    public partial class SelfInfo : System.Web.UI.Page
    {
        UserManage usermanage = new UserManage();
        protected void Page_Load(object sender, EventArgs e)
        {
            //若用户未登录，则跳转至登陆页面
            if (Session["userid"] == null)
            {
                Response.Redirect("login.aspx");
            }
            if (!IsPostBack)
            {   
                //设置用户ID，并根据用户ID获取个人信息
                usermanage.Id = Session["userid"].ToString();
                Getinfo();
                if (Session["username"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                else
                {
                    //设置顶部工具栏个人信息按钮文字为用户名
                    ((LinkButton)Master.FindControl("self")).Text = Session["username"].ToString();
                }

            }               
        }

        protected void  Getinfo()
        {
            //根据用户ID获取用户个人信息
            DataSet ds = usermanage.GetUserById(usermanage, "用户信息表");
            info_id.Text = Session["userid"].ToString();
            info_password.Text = ds.Tables[0].Rows[0][1].ToString();
            info_name.Text = ds.Tables[0].Rows[0][2].ToString();
            info_age.Text = ds.Tables[0].Rows[0][3].ToString();
            info_email.Text = ds.Tables[0].Rows[0][5].ToString();
            info_type.Text = ds.Tables[0].Rows[0][7].ToString();
            //保存个人信息至Session
            Session.Add("username", ds.Tables[0].Rows[0][2].ToString());
        }
        protected void ShowCard(object sender, EventArgs e)
        {
            //显示详细用户信息卡片
            AllInfoCard.Visible = true;
            ShowBtn.Enabled = false;
        }
        protected void HideCard(object sender, EventArgs e)
        {
            //隐藏详细用户信息卡片
            ShowBtn.Enabled = true;
            AllInfoCard.Visible = false;
        }
    }
}
