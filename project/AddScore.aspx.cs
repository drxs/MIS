﻿using System;
using System.Web.UI.WebControls;

namespace project
{
    public partial class AddScore : System.Web.UI.Page
    {
        ScoreManage scoremanage = new ScoreManage();
        protected void Page_Load(object sender, EventArgs e)
        {
            //若用户未登录，则跳转至登陆页面
            if (Session["userid"] == null)
            {
                Response.Redirect("login.aspx");
            }
            //判断用户权限
            if (Session["type"] != null && Session["type"].ToString() == "学生")
            {
                Response.Write("<script>alert('无权限访问此页面！'); window.history.go(-1);</script>");
            }
            if (!IsPostBack)
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                else
                {
                    //设置顶部工具栏个人信息按钮文字为用户名
                    ((LinkButton)Master.FindControl("self")).Text = Session["username"].ToString();
                }
            }
        }

        protected void Add_ScoreBtn(object sender, EventArgs e)
        {
            //添加按钮点击事件
            try
            {
                scoremanage.CourseId = add_course_id.SelectedValue;
                scoremanage.StudentId = add_id.SelectedValue;
                scoremanage.Score = add_score.Value;
                scoremanage.AddScore(scoremanage);
                //添加成功则执行下述语句，失败则执行catch下语句
                Response.Write("<script>alert('添加成功！'); location.href = './AllScore.aspx#nav_score';</script>");

            }
            catch
            {
                Response.Write("<script>alert('信息填写错误，请重新填写！')</script>");
            }
        }
    }
}