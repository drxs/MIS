﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="TeachingCourses.aspx.cs" Inherits="project.TeachingCourses" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>当前讲授课程--教务管理系统</title>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">当前讲授课程</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <asp:Label ID="Label1" runat="server" Text="loading" Font-Size="18px"></asp:Label>
            </div>
        </div>
        <br />
        <div class="mdui-valign">
            <div class="mdui-center mdui-text-center mdui-shadow-18">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="课程编号" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" CssClass="mdui-table mdui-center" ShowHeaderWhenEmpty="True" EmptyDataText="未查询到您当前讲授的课程信息！" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-BorderStyle="NotSet" EmptyDataRowStyle-CssClass="mdui-color-light-blue-50">
                    <AlternatingRowStyle BackColor="#B3E5FC" />
                    <Columns>
                        <asp:BoundField DataField="课程编号" HeaderText="课程编号" ReadOnly="True" SortExpression="课程编号" />
                        <asp:BoundField DataField="课程名称" HeaderText="课程名称" SortExpression="课程名称" />
                        <asp:BoundField DataField="课时" HeaderText="课时" SortExpression="课时" />
                        <asp:BoundField DataField="学分" HeaderText="学分" SortExpression="学分" />
                        <asp:BoundField DataField="教室" HeaderText="教室" SortExpression="教室" />
                        <asp:BoundField DataField="上课时间" HeaderText="上课时间" SortExpression="上课时间" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#e0f7fa" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [课程编号], [课程名称], [课时], [学分], [教室], [上课时间] FROM [开课信息管理] WHERE ([授课教师] = @授课教师)">
                    <SelectParameters>
                        <asp:SessionParameter Name="授课教师" SessionField="username" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <br />
    </div>
</asp:Content>