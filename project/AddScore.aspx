﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AddScore.aspx.cs" Inherits="project.AddScore" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>添加成绩信息--教务管理系统</title>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">添加新成绩</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label1" runat="server" Text="学生姓名" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_name" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Name" CssClass="mdui-select" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [Name] FROM [学生信息表]"></asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" hidden="hidden">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label2" runat="server" Text="学生ID" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_id" runat="server" DataSourceID="SqlDataSource2" DataTextField="Id" DataValueField="Id" CssClass="mdui-select" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [Id] FROM [学生信息表] WHERE ([Name] = @Name)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="add_name" Name="Name" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label3" runat="server" Text="课程名称" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="DropDownList4" runat="server" DataSourceID="SqlDataSource3" DataTextField="CourseName" DataValueField="CourseName" AutoPostBack="True" CssClass="mdui-select"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [CourseName] FROM [课程信息表] ORDER BY [CourseId]"></asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" hidden="hidden">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label4" runat="server" Text="课程ID" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_course_id" runat="server" DataSourceID="SqlDataSource4" DataTextField="CourseId" DataValueField="CourseId" AutoPostBack="True" CssClass="mdui-select"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [CourseId] FROM [课程信息表] WHERE ([CourseName] = @CourseName) ORDER BY [CourseId]">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList4" Name="CourseName" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label5" runat="server" Text="成绩：（例：90）" CssClass="mdui-textfield-label"></asp:Label>
                <textarea class="mdui-textfield-input" runat="server" id="add_score"></textarea>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-3 mdui-center mdui-text-center">
                <asp:Button ID="Button1" runat="server" Text="添加" OnClick="Add_ScoreBtn" CssClass="mdui-btn mdui-btn-block mdui-shadow-9 mdui-btn-raised mdui-ripple mdui-color-light-blue mdui-text-color-white" />
            </div>
        </div>
        <br />
    </div>
</asp:Content>