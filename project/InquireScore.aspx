﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="InquireScore.aspx.cs" Inherits="project.InquireScore" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>成绩查询--教务管理系统</title>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">成绩查询</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <asp:Label ID="Label1" runat="server" Text="loading" Font-Size="18px"></asp:Label>
            </div>
        </div>
        <br />
        <div class="mdui-valign">
            <div class="mdui-center mdui-text-center mdui-shadow-18">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="#333333" GridLines="None" CssClass="mdui-table mdui-center" DataKeyNames="课程编号" ShowHeaderWhenEmpty="True" EmptyDataText="未查询到成绩信息！" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-BorderStyle="NotSet" EmptyDataRowStyle-CssClass="mdui-color-light-blue-50">
                    <AlternatingRowStyle BackColor="#B3E5FC" />
                    <Columns>
                        <asp:BoundField DataField="学生姓名" HeaderText="学生姓名" SortExpression="学生姓名" />
                        <asp:BoundField DataField="课程名称" HeaderText="课程名称" SortExpression="课程名称" />
                        <asp:BoundField DataField="课程编号" HeaderText="课程编号" SortExpression="课程编号" ReadOnly="True" />
                        <asp:BoundField DataField="课时" HeaderText="课时" SortExpression="课时" />
                        <asp:BoundField DataField="学分" HeaderText="学分" SortExpression="学分" />
                        <asp:BoundField DataField="授课教师" HeaderText="授课教师" SortExpression="授课教师" />
                        <asp:BoundField DataField="教师职称" HeaderText="教师职称" SortExpression="教师职称" />
                        <asp:BoundField DataField="成绩" HeaderText="成绩" SortExpression="成绩" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#e0f7fa" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT * FROM [成绩查询] WHERE ([学生姓名] = @学生姓名)">
                    <SelectParameters>
                        <asp:SessionParameter Name="学生姓名" SessionField="username" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <br />
    </div>
</asp:Content>