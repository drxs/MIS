﻿using System;
using System.Web.UI.WebControls;

namespace project
{
    public partial class AddCourse : System.Web.UI.Page
    {
        CourseManage coursemanage = new CourseManage();
        protected void Page_Load(object sender, EventArgs e)
        {
            //若用户未登录，则跳转至登陆页面
            if (Session["userid"] == null)
            {
                Response.Redirect("login.aspx");
            }
            //判断用户权限
            if(Session["type"] != null && Session["type"].ToString() == "学生")
            {
                Response.Write("<script>alert('无权限访问此页面！'); window.history.go(-1);</script>");
            }
            if (!IsPostBack)
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                else
                {
                    //设置顶部工具栏个人信息按钮文字为用户名
                    ((LinkButton)Master.FindControl("self")).Text = Session["username"].ToString();                                     
                }
            }
        }
        protected void btn_add(object sender, EventArgs e)
        {
            //添加按钮点击事件
            try { 
            coursemanage.CourseId = add_courseid.Value;
            coursemanage.CourseName = add_coursename.Value;
            coursemanage.Hour = add_hour.Value;
            coursemanage.Credit = add_credit.Value;
            coursemanage.TeacherId = add_teacher_id.SelectedValue;
            coursemanage.ClassroomId = add_classroom_id.SelectedValue;
            coursemanage.Time = add_time.Value;
            coursemanage.AddCourse(coursemanage);
            //添加成功则执行下述语句，失败则执行catch下语句
            Response.Write("<script>alert('添加成功！'); location.href = './AllCourseInfo.aspx#nav_courses';</script>");
            }
            catch
            {
                Response.Write("<script>alert('信息填写错误，请重新填写！')</script>");
            }
        }
    }
}