﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SelfInfo.aspx.cs" Inherits="project.SelfInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>个人信息管理--教务管理系统</title>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container">
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">个人信息查询</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-row mdui-center mdui-valign">
            <div class="mdui-col-xs-1 mdui-center"></div>
            <div class="mdui-col-xs-4 mdui-center">
                <div class="mdui-card">
                    <!-- 卡片的媒体内容，可以包含图片、视频等媒体内容，以及标题、副标题 -->
                    <div class="mdui-card-media">
                        <img src="https://file.wangyusong.cn/MIS/md_picture_1.jpg" />
                        <div class="mdui-card-media-covered">
                            <div class="mdui-card-primary">
                                <div class="mdui-card-primary-title">基本个人信息</div>
                            </div>
                        </div>
                    </div>
                    <!-- 卡片的内容 -->
                    <div class="mdui-card-content">
                        <asp:Label ID="label_id" runat="server" Text="用户编号:" Font-Size="17px" CssClass="mdui-textfield-label"></asp:Label>
                        <asp:TextBox ID="info_id" runat="server" CssClass="mdui-textfield-input" Enabled="False"></asp:TextBox>
                        <br />
                        <asp:Label ID="label_name" runat="server" Text="用户姓名：" Font-Size="17px" CssClass="mdui-textfield-label"></asp:Label>
                        <asp:TextBox ID="info_name" runat="server" CssClass="mdui-textfield-input" Enabled="False"></asp:TextBox>
                        <br />
                        <asp:Label ID="label_type" runat="server" Text="用户类型：" Font-Size="17px" CssClass="mdui-textfield-label"></asp:Label>
                        <asp:TextBox ID="info_type" runat="server" CssClass="mdui-textfield-input" Enabled="False"></asp:TextBox>
                    </div>
                    <!-- 卡片的按钮 -->
                    <div class="mdui-card-actions">
                        <asp:Button ID="ShowBtn" runat="server" Text="其他详细信息" OnClick="ShowCard" CssClass="mdui-btn mdui-btn-raised mdui-ripple mdui-float-right mdui-color-theme mdui-text-color-white" Enabled="false" />
                    </div>
                </div>
            </div>
            <!--详细信息卡片-->
            <div class="mdui-col-xs-4 mdui-center" runat="server" id="AllInfoCard">
                <div class="mdui-card">
                    <!-- 卡片的媒体内容，可以包含图片、视频等媒体内容，以及标题、副标题 -->
                    <div class="mdui-card-media">
                        <img src="https://file.wangyusong.cn/MIS/md_picture_2.jpg" />
                        <div class="mdui-card-media-covered">
                            <div class="mdui-card-primary">
                                <div class="mdui-card-primary-title">其他个人信息</div>
                            </div>
                        </div>
                    </div>
                    <!-- 卡片的内容 -->
                    <div class="mdui-card-content">
                        <asp:Label ID="label_password" runat="server" Text="用户密码:" Font-Size="17px" CssClass="mdui-textfield-label"></asp:Label>
                        <asp:TextBox ID="info_password" runat="server" CssClass="mdui-textfield-input" Enabled="False"></asp:TextBox>
                        <br />
                        <asp:Label ID="label_age" runat="server" Text="用户年龄：" Font-Size="17px" CssClass="mdui-textfield-label"></asp:Label>
                        <asp:TextBox ID="info_age" runat="server" CssClass="mdui-textfield-input" Enabled="False"></asp:TextBox>
                        <br />
                        <asp:Label ID="label_email" runat="server" Text="电子邮件：" Font-Size="17px" CssClass="mdui-textfield-label"></asp:Label>
                        <asp:TextBox ID="info_email" runat="server" CssClass="mdui-textfield-input" Enabled="False"></asp:TextBox>
                    </div>
                    <!-- 卡片的按钮 -->
                    <div class="mdui-card-actions">
                        <asp:Button ID="HideBtn" runat="server" Text="收起详细信息" OnClick="HideCard" CssClass="mdui-btn mdui-btn-raised mdui-ripple mdui-float-right mdui-color-theme mdui-text-color-white" />
                    </div>
                </div>
            </div>
            <div class="mdui-col-xs-1 mdui-center"></div>
        </div>
        <br />
    </div>
</asp:Content>