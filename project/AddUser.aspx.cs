﻿using System;
using System.Web.UI.WebControls;

namespace project
{
    public partial class AddUser : System.Web.UI.Page
    {
        UserManage usermanage = new UserManage();
        protected void Page_Load(object sender, EventArgs e)
        {
            //若用户未登录，则跳转至登陆页面
            if (Session["userid"] == null)
            {
                Response.Redirect("login.aspx");
            }
            //判断用户权限
            if (Session["type"] != null && Session["type"].ToString() == "学生")
            {
                Response.Write("<script>alert('无权限访问此页面！'); window.history.go(-1);</script>");
            }
            if (!IsPostBack)
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                else
                {
                    //设置顶部工具栏个人信息按钮文字为用户名
                    ((LinkButton)Master.FindControl("self")).Text = Session["username"].ToString();
                }
            }

            //根据所选用户类型决定相关部件的可见性
            if (add_type.SelectedValue == "教师")
            {
                label_title.Visible = true;
                label_class.Visible = false;
            }
            if(add_type.SelectedValue=="学生")
            {
                label_title.Visible = false;
                label_class.Visible = true;
            }
        }
        protected void AddBtn(object sender, EventArgs e)
        {
            //添加按钮点击事件
            try
            {
                usermanage.AddId = add_userid.Value;
                usermanage.Name = add_name.Value;
                usermanage.Password = add_password.Value;
                usermanage.Age = add_age.Value;
                usermanage.Sex = add_sex.Text;
                usermanage.Email = add_email.Value;
                usermanage.Type = add_type.SelectedValue;
                usermanage.CollegeId = add_college_id.SelectedValue;
                usermanage.ClassId = add_class_id.SelectedValue;
                usermanage.Title = add_title.SelectedValue;
                //若所选用户类型为学生，则调用AddUser和AddStudent
                if (add_type.SelectedValue == "学生")
                {
                    usermanage.AddUser(usermanage);
                    usermanage.AddStudent(usermanage);
                    //添加成功则执行下述语句，失败则执行catch下语句
                    if (Session["type"].ToString() == "管理员") 
                    {
                        //若当前用户为管理员，添加成功则跳转至全部用户信息界面
                        Response.Write("<script>alert('添加成功！'); location.href = './AllUserInfo.aspx#nav_info';</script>");                        
                    }
                    else if(Session["type"].ToString() == "教师") 
                    {
                        //若当前用户为教师，添加成功则返回当前页面
                        Response.Write("<script>alert('添加成功！')</script>");
                    }
                    else
                    {
                        Response.Write("<script>alert('系统错误！')</script>");
                    }
                }
                //若所选用户类型为教师，则调用AddUser和AddTeacher
                else
                {
                    usermanage.AddUser(usermanage);
                    usermanage.AddTeacher(usermanage);
                    //添加成功则执行下述语句，失败则执行catch下语句
                    if (Session["type"].ToString() == "管理员")
                    {
                        Response.Write("<script>alert('添加成功！'); location.href = './AllUserInfo.aspx#nav_info';</script>");
                    }
                    else if (Session["type"].ToString() == "教师")
                    {
                        Response.Write("<script>alert('添加成功！')</script>");
                    }
                    else
                    {
                        Response.Write("<script>alert('系统错误！')</script>");
                    }
                }
            }
            catch
            {
                Response.Write("<script>alert('信息填写错误，请重新填写！')</script>");
            }
        }
    }
}
