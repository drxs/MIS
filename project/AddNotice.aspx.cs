﻿using System;
using System.Data;
using System.Web.UI.WebControls;

namespace project
{
    public partial class AddNotice : System.Web.UI.Page
    {
        NoticeManage noticemanage = new NoticeManage();
        private int num = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //若用户未登录，则跳转至登陆页面
            if (Session["userid"] == null)
            {
                Response.Redirect("login.aspx");
            }
            //获取数据库中公告总数量，并自动生成公告编号
            DataSet data = noticemanage.GetAll(noticemanage);
            num = data.Tables[0].Rows.Count + 1;
            //判断用户权限
            if (Session["type"] != null && Session["type"].ToString() == "学生")
            {
                Response.Write("<script>alert('无权限访问此页面！'); window.history.go(-1);</script>");
            }
            if (!IsPostBack)
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                else
                {
                    //设置顶部工具栏个人信息按钮文字为用户名
                    ((LinkButton)Master.FindControl("self")).Text = Session["username"].ToString();
                    add_time.Text = DateTime.Now.ToString("yyyy-MM-dd");
                    add_id.Text = num.ToString();
                }
            }
        }

        protected void Add_NoticeBtn(object sender, EventArgs e)
        {
            //添加按钮点击事件
            try
            {
                noticemanage.NoticeId = add_id.Text;
                noticemanage.PublisherId = Session["userid"].ToString();
                noticemanage.Time = add_time.Text;
                noticemanage.Contents = add_content.Value;
                noticemanage.AddNotice(noticemanage);
                //添加成功则执行下述语句，失败则执行catch下语句
                Response.Write("<script>alert('添加成功！'); location.href = './InquireNotice.aspx#nav_notice';</script>");
            }
            catch
            {
                Response.Write("<script>alert('信息填写错误，请重新填写！')</script>");
            }
        }
    }
}