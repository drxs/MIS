﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="project.login" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>教务管理系统--用户登录</title>
    <link rel="stylesheet" type="text/css" href="https://file.wangyusong.cn/MIS/login/default.css" />
    <link rel="stylesheet" type="text/css" href="https://file.wangyusong.cn/MIS/login/styles.css" />
    <link rel="shortcut icon" href="https://file.wangyusong.cn/favicon.ico" />
</head>
<body>
    <form id="form1" runat="server">
        <article class="htmleaf-container">
            <header class="htmleaf-header">
                <h1>教务管理系统--用户登录页面</h1>
                <h1><span><b>Educational Management System </b></span></h1>
                <div class="htmleaf-links">
                </div>
            </header>
            <div class="panel-lite">
                <div class="thumbur">
                    <div class="icon-lock"></div>
                </div>
                <h4>用户登录</h4>
                <div class="form-group">
                    <input required="required" class="form-control" runat="server" id="loginid" autocomplete="off" />
                    <asp:Label ID="Label1" runat="server" Text="用户ID" CssClass="form-label"></asp:Label>
                </div>
                <div class="form-group">
                    <input type="password" required="required" class="form-control" runat="server" id="loginpassword" autocomplete="new-password" />
                    <asp:Label ID="Label2" runat="server" Text="密　码" CssClass="form-label"></asp:Label>
                </div>
                <br />
                <div>
                    <asp:CheckBox ID="auto_login" runat="server" Text="3天内自动登录" />
                </div>
                <a>2016001(学生)&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;2012005(教师)</a>
                <asp:Button ID="Button1" runat="server" OnClick="BtnLogin_Click" CssClass="floating-btn" Text="Login" Font-Size="20px" />
            </div>
        </article>
    </form>
</body>
</html>
