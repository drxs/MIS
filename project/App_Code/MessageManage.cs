﻿using System.Data;
using System.Data.SqlClient;
using project;

/// <summary>
/// 实现获取私信、发送私信功能
/// </summary>
public class MessageManage
{
    public MessageManage()
    {
        //
        // TODO: 在此处添加构造函数逻辑
        //
    }
    DataBase data = new DataBase();

    #region 定义数据结构
    private string messageid = "";
    private string fromid = "";
    private string receiveid = "";
    private string contenttext = "";
    private string time = "";
    private string receivename = "";
    private string fromname = "";

    public string MessageId
    {
        get { return messageid; }
        set { messageid = value; }
    }
    public string FromId
    {
        get { return fromid; }
        set { fromid = value; }
    }
    public string ReceiveId
    {
        get { return receiveid; }
        set { receiveid = value; }
    }
    public string ContentText
    {
        get { return contenttext; }
        set { contenttext = value; }
    }
    public string Time
    {
        get { return time; }
        set { time = value; }
    }
    public string ReceiveName
    {
        get { return receivename; }
        set { receivename = value; }
    }
    public string FromName
    {
        get { return fromname; }
        set { fromname = value; }
    }
    #endregion

    #region 根据ID获取私信列表
    public DataSet GetMessageById(MessageManage messagemanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@receiveid",  SqlDbType.NChar, 20, messagemanage.ReceiveId +"%"),
            };
        return (data.RunProcReturn("select * from 私信信息表 where receiveid like @receiveid", prams,"私信信息表"));
    }
    #endregion

    #region 获取全部私信
    public DataSet GetAll(MessageManage messagemanage) 
    {
        return (data.RunProcReturn("select * from 私信信息表","私信信息表"));
    }

    #endregion

    #region 发送私信
    public int SendMessage(MessageManage messagemanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@messageid",  SqlDbType.NChar, 20, messagemanage.MessageId),
            data.MakeInParam("@fromid",  SqlDbType.NChar, 20,messagemanage.FromId),
            data.MakeInParam("@receiveid",  SqlDbType.NChar, 20, messagemanage.ReceiveId),
            data.MakeInParam("@contenttext",  SqlDbType.NChar, 300, messagemanage.ContentText),
            data.MakeInParam("@time",  SqlDbType.NChar, 20, messagemanage.Time),
            data.MakeInParam("@receivename",  SqlDbType.NChar, 20, messagemanage.ReceiveName),
            data.MakeInParam("@fromname",  SqlDbType.NChar, 20, messagemanage.FromName),
            };
        return (data.RunProc("INSERT INTO 私信信息表 (messageid,fromid,receiveid,contenttext,time,receivename,fromname) VALUES(@messageid,@fromid,@receiveid,@contenttext,@time,@receivename,@fromname)", prams));
    }
    #endregion
}
