﻿using System.Data;
using System.Data.SqlClient;
using project;

/// <summary>
/// 实现添加课程功能
/// </summary>

public class CourseManage
{
    public CourseManage()
    {
        //
        // TODO: 在此处添加构造函数逻辑
        //   
    }

    DataBase data = new DataBase();

    #region 定义数据结构
    private string courseid = "";
    private string coursename = "";
    private string hour = "";
    private string credit = "";
    private string teachername = "";
    private string teacherid = "";
    private string title = "";
    private string classroomname = "";
    private string classroomid = "";
    private string time = "";

    public string CourseId
    {
        get { return courseid; }
        set { courseid = value; }
    }
    public string CourseName
    {
        get { return coursename; }
        set { coursename = value; }
    }
    public string Hour
    {
        get { return hour; }
        set { hour = value; }
    }
    public string Credit
    {
        get { return credit; }
        set { credit = value; }
    }
    public string TeacherName
    {
        get { return teachername; }
        set { teachername = value; }
    }
    public string TeacherId
    {
        get { return teacherid; }
        set { teacherid = value;  }
    }
    public string Title
    {
        get { return title; }
        set { title = value; }
    }
    public string ClassroomName
    {
        get { return classroomname; }
        set { classroomname = value; }
    }
    public string ClassroomId
    {
        get { return classroomid; }
        set { classroomid = value; }
    }
    public string Time
    {
        get { return time; }
        set { time = value; }
    }
    #endregion

    #region 添加课程
    /// <summary>
    /// 添加课程信息
    /// </summary>
    /// <param name="courseinfo"></param>
    /// <returns></returns>
    public int AddCourse(CourseManage coursemanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@courseid",  SqlDbType.NChar, 20, coursemanage.CourseId),
            data.MakeInParam("@coursename",  SqlDbType.NChar, 20,coursemanage.CourseName),
            data.MakeInParam("@hour",  SqlDbType.NChar, 10, coursemanage.Hour),
            data.MakeInParam("@credit",  SqlDbType.NChar, 10, coursemanage.Credit),
            data.MakeInParam("@teacherid",  SqlDbType.NChar, 20, coursemanage.TeacherId),
            data.MakeInParam("@classroomid",  SqlDbType.NChar, 20, coursemanage.ClassroomId),
            data.MakeInParam("@time",  SqlDbType.NChar, 20, coursemanage.Time),
            };
        return (data.RunProc("INSERT INTO 课程信息表 (courseid,coursename,hour,credit,teacherid,classroomid,time) VALUES(@courseid,@coursename,@hour,@credit,@teacherid,@classroomid,@time)", prams));
    }
    #endregion
}
