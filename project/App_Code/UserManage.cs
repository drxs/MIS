﻿using System.Data;
using System.Data.SqlClient;
using project;

/// <summary>
/// 实现添加用户、添加学生、添加教师、用户登录、查询用户信息、修改用户信息等功能
/// </summary>

public class UserManage
{
    public UserManage()
    {
        //
        // TODO: 在此处添加构造函数逻辑
        //
    }

    DataBase data = new DataBase();

    #region 定义数据结构
    private string id = "";
    private string addid = "";
    private string name = "";
    private string age = "";
    private string password = "";
    private string sex = "";
    private string email = "";
    private string tel = "";
    private string type = "";
    private string collegeid = "";
    private string classid = "";
    private string title ="";

    public string Id
    {
        get { return id; }
        set { id = value; }
    }
    public string AddId
    {
        get { return addid; }
        set { addid = value; }
    }
    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    public string Age
    {
        get { return age; }
        set { age = value; }
    }
    public string Password
    {
        get { return password; }
        set { password = value; }
    }
    public string Sex
    {
        get { return sex; }
        set { sex = value; }
    }
    public string Email
    {
        get { return email; }
        set { email = value; }
    }
    public string Tel
    {
        get { return tel; }
        set { tel = value; }
    }
    public string Type
    {
        get { return type; }
        set { type = value; }
    }
    public string CollegeId
    {
        get { return collegeid; }
        set { collegeid = value; }
    }
    public string ClassId
    {
        get { return classid; }
        set { classid = value; }
    }
    public string Title
    {
        get { return title; }
        set { title = value; }
    }
    #endregion

    #region 添加用户
    /// <summary>
    /// 添加用户信息
    /// </summary>
    /// <param name="usermanage"></param>
    /// <returns></returns>
    public int AddUser(UserManage usermanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@addid",  SqlDbType.NChar, 20, usermanage.AddId),
            data.MakeInParam("@name",  SqlDbType.NChar, 20,usermanage.Name),
            data.MakeInParam("@age",  SqlDbType.NChar, 10,usermanage.Age),
            data.MakeInParam("@password",  SqlDbType.NChar, 30, usermanage.Password),
            data.MakeInParam("@sex",  SqlDbType.NChar, 10, usermanage.Sex),
            data.MakeInParam("@email",  SqlDbType.NChar, 30, usermanage.Email),
            data.MakeInParam("@tel",  SqlDbType.NChar, 11, usermanage.Tel),
            data.MakeInParam("@type",  SqlDbType.NChar, 10, usermanage.Type),
            };
        return (data.RunProc("INSERT INTO 用户信息表 (id,name,age,password,sex,email,tel,type) VALUES(@addid,@name,@age,@password,@sex,@email,@tel,@type)", prams));
    }
    #endregion

    #region 添加学生
    /// <summary>
    /// 添加学生信息
    /// </summary>
    /// <param name="usermanage"></param>
    /// <returns></returns>
    public int AddStudent(UserManage usermanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@addid",  SqlDbType.NChar, 20, usermanage.AddId),
            data.MakeInParam("@name",  SqlDbType.NChar, 20,usermanage.Name),
            data.MakeInParam("@age",  SqlDbType.NChar, 10,usermanage.Age),
            data.MakeInParam("@sex",  SqlDbType.NChar, 10, usermanage.Sex),
            data.MakeInParam("@collegeid",  SqlDbType.NChar, 20, usermanage.CollegeId),
            data.MakeInParam("@classid",  SqlDbType.NChar, 20, usermanage.ClassId),
            };
        return (data.RunProc("INSERT INTO 学生信息表 (id,name,age,sex,collegeid,classid) VALUES(@addid,@name,@age,@sex,@collegeid,@classid)", prams));
    }

    #endregion

    #region 添加教师
    /// <summary>
    /// 添加教师信息
    /// </summary>
    /// <param name="usermanage"></param>
    /// <returns></returns>
    public int AddTeacher(UserManage usermanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@addid",  SqlDbType.NChar, 20, usermanage.AddId),
            data.MakeInParam("@name",  SqlDbType.NChar, 20,usermanage.Name),
            data.MakeInParam("@age",  SqlDbType.NChar, 10,usermanage.Age),
            data.MakeInParam("@sex",  SqlDbType.NChar, 10, usermanage.Sex),
            data.MakeInParam("@collegeid",  SqlDbType.NChar, 20, usermanage.CollegeId),
            data.MakeInParam("@title",  SqlDbType.NChar, 10, usermanage.Title),
            };
        return (data.RunProc("INSERT INTO 教师信息表 (id,name,age,sex,collegeid,title) VALUES(@addid,@name,@age,@sex,@collegeid,@title)", prams));
    }
    #endregion

    #region 查询用户信息
    /// <summary>
    /// 根据用户Id查询用户信息
    /// </summary>
    /// <param name="usermanage"></param>
    /// <param name="tbName"></param>
    /// <returns></returns>
    public DataSet GetUserById(UserManage usermanage, string tbName)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@id",  SqlDbType.NChar, 20, usermanage.Id +"%"),
            };
        return (data.RunProcReturn("select * from 用户信息表 where id like @id", prams, tbName));
    }

    /// <summary>
    /// 查询所有用户信息
    /// </summary>
    /// <param name="tbName"></param>
    /// <returns></returns>
    public DataSet GetAllUser(string tbName)
    {
        return (data.RunProcReturn("select * from 用户信息表 ORDER BY id", tbName));
    }
    #endregion

    #region 修改用户信息
    /// <summary>
    /// 修改用户信息
    /// </summary>
    /// <param name="usermanage"></param>
    /// <returns></returns>
    public int UpdateUser(UserManage usermanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@id",  SqlDbType.NChar, 20, usermanage.Id),
            data.MakeInParam("@password",  SqlDbType.NChar, 30, usermanage.Password),
            data.MakeInParam("@name",  SqlDbType.NChar, 20,usermanage.Name),
            data.MakeInParam("@age", SqlDbType.NChar, 10, usermanage.Age),
            data.MakeInParam("@sex",  SqlDbType.NChar, 10, usermanage.Sex),
            data.MakeInParam("@email",  SqlDbType.NChar, 30, usermanage.Email),
            data.MakeInParam("@tel",  SqlDbType.NChar, 11, usermanage.Tel),
            };
        return (data.RunProc("update 用户信息表 set password=@password,name=@name, age=@age, sex=@sex,email=@email,tel=@tel where id=@id", prams));
    }
    #endregion

    #region 用户登录
    /// <summary>
    /// 用户登录
    /// </summary>
    /// <param name="usermanage"></param>
    /// <returns></returns>
    public DataSet UserLogin(UserManage usermanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@id",  SqlDbType.NChar, 20, usermanage.Id ),
            data.MakeInParam("@password",  SqlDbType.NChar, 30,usermanage.Password ),
            };
        return (data.RunProcReturn("SELECT * FROM 用户信息表 WHERE (id = @id) AND (password = @password)", prams, "用户信息表"));
    }
    #endregion

}
