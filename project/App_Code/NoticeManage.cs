﻿using System.Data;
using System.Data.SqlClient;
using project;

/// <summary>
/// 实现发布公告功能
/// </summary>

public class NoticeManage
{
    public NoticeManage() 
    {
        //
        // TODO: 在此处添加构造函数逻辑
        //
    }
    DataBase data = new DataBase();

    #region 定义数据结构
    private string noticeid = "";
    private string contents = "";
    private string time = "";
    private string publisherid = "";

    public string NoticeId
    {
        get { return noticeid; }
        set { noticeid = value; }
    }
    public string Contents 
    {
        get { return contents; }
        set { contents = value; }
    }
    public string Time
    {
        get { return time; }
        set { time = value; }
    }
    public string PublisherId
    {
        get { return publisherid; }
        set { publisherid = value; }
    }
    #endregion

    #region 发布公告
    public int AddNotice(NoticeManage noticemanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@noticeid",  SqlDbType.NChar, 20, noticemanage.NoticeId),
            data.MakeInParam("@contents",  SqlDbType.NChar, 300,noticemanage.Contents),
            data.MakeInParam("@time",  SqlDbType.NChar, 20, noticemanage.Time),
            data.MakeInParam("@publisherid",  SqlDbType.NChar, 20, noticemanage.PublisherId),
            };
        return (data.RunProc("INSERT INTO 公告信息表 (noticeid,contents,time,publisherid) VALUES(@noticeid,@contents,@time,@publisherid)", prams));
    }
    #endregion

    #region 获取全部公告
    public DataSet GetAll(NoticeManage noticemanage)
    {
        return (data.RunProcReturn("select * from 公告信息表", "公告信息表"));
    }



    #endregion

}