﻿using System.Data;
using System.Data.SqlClient;
using project;

/// <summary>
/// 实现添加成绩信息功能
/// </summary>

public class ScoreManage
    {
    public ScoreManage()
    {
        //
        // TODO: 在此处添加构造函数逻辑
        //
    }
    DataBase data = new DataBase();

    #region 定义数据结构

    private string studentid = "";
    private string studentname = "";
    private string courseid = "";
    private string coursename = "";
    private string credit = "";
    private string score = "";

    public string StudentId
    {
        get { return studentid; }
        set { studentid = value; }
    }
    public string StudentName
    {
        get { return studentname; }
        set { studentname = value; }
    }
    public string CourseId
    {
        get { return courseid; }
        set { courseid = value; }
    }
    public string CourseName
    {
        get { return coursename; }
        set { coursename = value; }
    }
    public string Credit
    {
        get { return credit; }
        set { credit = value; }
    }
    public string Score
    {
        get { return score; }
        set { score = value; }
    }
    #endregion

    #region 添加成绩信息
    /// <summary>
    /// 添加成绩信息
    /// </summary>
    /// <param name="scoremanage"></param>
    /// <returns></returns>
    public int AddScore(ScoreManage scoremanage)
    {
        SqlParameter[] prams = {
            data.MakeInParam("@studentid",  SqlDbType.NChar, 20, scoremanage.StudentId),
            data.MakeInParam("@courseid",  SqlDbType.NChar, 20, scoremanage.CourseId),
            data.MakeInParam("@score",  SqlDbType.NChar, 10, scoremanage.Score),
            };
        return (data.RunProc("INSERT INTO 成绩信息表 (studentid,courseid,score) VALUES(@studentid,@courseid,@score)", prams));
    }
    #endregion

}
