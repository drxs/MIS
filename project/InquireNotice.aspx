﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="InquireNotice.aspx.cs" Inherits="project.InquireNotice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <title>查询公告--教务管理系统</title>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">查询公告</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-valign">
            <div class="mdui-center mdui-text-center mdui-shadow-18">
                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="公告编号" DataSourceID="SqlDataSource1" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="mdui-table mdui-center" ShowHeaderWhenEmpty="True" EmptyDataText="暂无公告！" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-BorderStyle="NotSet" EmptyDataRowStyle-CssClass="mdui-color-light-blue-50">
                    <AlternatingRowStyle BackColor="#B3E5FC" />
                    <Columns>
                        <asp:BoundField DataField="公告编号" HeaderText="公告编号" ReadOnly="True" SortExpression="公告编号" />
                        <asp:BoundField DataField="发布时间" HeaderText="发布时间" SortExpression="发布时间" />
                        <asp:BoundField DataField="发布者" HeaderText="发布者" SortExpression="发布者" />
                        <asp:BoundField DataField="内容" HeaderText="内容" SortExpression="内容" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#e0f7fa" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT * FROM [查询公告] ORDER BY [公告编号] DESC"></asp:SqlDataSource>
            </div>
        </div>
        <br />
    </div>
</asp:Content>