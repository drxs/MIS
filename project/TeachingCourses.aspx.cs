﻿using System;
using System.Web.UI.WebControls;

namespace project
{
    public partial class TeachingCourses : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //若用户未登录，则跳转至登陆页面
            if (Session["userid"] == null)
            {
                Response.Redirect("login.aspx");
            }
            //判断用户权限
            if ((Session["type"] != null && Session["type"].ToString() == "学生")|| (Session["type"] != null && Session["type"].ToString() == "管理员"))
            {
                Response.Write("<script>alert('无权限访问此页面！'); window.history.go(-1);</script>");
            }
            if (!IsPostBack)
            {
                if(Session["username"]==null)
                {
                    Response.Redirect("login.aspx");
                }
                else
                {
                    //设置顶部工具栏个人信息按钮文字为用户名
                    ((LinkButton)Master.FindControl("self")).Text = Session["username"].ToString();
                    //设置标签文字
                    Label1.Text = "当前用户ID：" + Session["userid"];
                }                
            }
        }
    }
}