﻿using System;
using System.Web.UI.WebControls;
using System.Data;

namespace project
{
    public partial class SendMessage : System.Web.UI.Page
    {
        MessageManage messagemanage = new MessageManage();
        private int num = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            //若用户未登录，则跳转至登陆页面
            if (Session["userid"] == null)
            {
                Response.Redirect("login.aspx");
            }
            //获取数据库中私信总数量，并自动生成私信编号
            DataSet data = messagemanage.GetAll(messagemanage);
            num = data.Tables[0].Rows.Count + 1;
            if (!IsPostBack)
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                else
                {
                    //设置顶部工具栏个人信息按钮文字为用户名
                    ((LinkButton)Master.FindControl("self")).Text = Session["username"].ToString();
                    //设置标签文字
                    Label1.Text = "当前用户ID：" + Session["userid"];
                    Label2.Text ="系统日期：" + DateTime.Now.ToString("yyyy-MM-dd");
                    //私信发送日期由系统自动填充
                    add_time.Text = DateTime.Now.ToString("yyyy-MM-dd");
                }
            }
        }
        protected void Send_Btn(object sender, EventArgs e)
        {
            //添加按钮点击事件            
           try
            {
                messagemanage.MessageId = num.ToString();
                messagemanage.FromId = Session["userid"].ToString();
                messagemanage.ReceiveId = DropDownList2.SelectedValue;
                messagemanage.Time = add_time.Text;
                messagemanage.ContentText = add_content.Value;
                messagemanage.FromName = Session["username"].ToString();
                messagemanage.ReceiveName = DropDownList1.SelectedValue;
                messagemanage.SendMessage(messagemanage);
                //发送成功则跳转至发件箱页面，失败则执行catch下语句
                Response.Write("<script>alert('发送成功！'); location.href = './SentBox.aspx#nav_message';</script>");
            }
            catch
            {
                Response.Write("<script>alert('信息填写错误，请重新填写！')</script>");
            }
        }
    }
}