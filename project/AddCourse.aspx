﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AddCourse.aspx.cs" Inherits="project.AddCourse" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>添加课程信息--教务管理系统</title>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">添加新课程</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label1" runat="server" Text="课程编号（例：2020001）" CssClass="mdui-textfield-label"></asp:Label>
                <textarea class="mdui-textfield-input" id="add_courseid" runat="server"></textarea>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label2" runat="server" Text="课程名称" CssClass="mdui-textfield-label"></asp:Label>
                <textarea class="mdui-textfield-input" id="add_coursename" runat="server"></textarea>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label3" runat="server" Text="课时（例：32））" CssClass="mdui-textfield-label"></asp:Label>
                <textarea class="mdui-textfield-input" id="add_hour" runat="server"></textarea>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label4" runat="server" Text="学分（例：2）" CssClass="mdui-textfield-label"></asp:Label>
                <textarea class="mdui-textfield-input" id="add_credit" runat="server"></textarea>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label5" runat="server" Text="授课教师" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="teacher" DataTextField="Name" DataValueField="Name" CssClass="mdui-select" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource ID="teacher" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [Name] FROM [教师信息表] ORDER BY [Id]"></asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" hidden="hidden">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label6" runat="server" Text="授课教师ID" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_teacher_id" runat="server" DataSourceID="SqlDataSource1" DataTextField="Id" DataValueField="Id" CssClass="mdui-select"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [Id] FROM [教师信息表] WHERE ([Name] = @Name)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" Name="Name" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label7" runat="server" Text="上课教室" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="DropDownList2" runat="server" DataSourceID="classroom" DataTextField="ClassroomName" DataValueField="ClassroomName" CssClass="mdui-select" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource ID="classroom" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [ClassroomName] FROM [教室信息表] ORDER BY [ClassroomId]"></asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" hidden="hidden">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label8" runat="server" Text="上课教室ID" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_classroom_id" runat="server" DataSourceID="SqlDataSource2" DataTextField="ClassroomId" DataValueField="ClassroomId" CssClass="mdui-select"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [ClassroomId] FROM [教室信息表] WHERE ([ClassroomName] = @ClassroomName)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList2" Name="ClassroomName" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" runat="server" id="div1">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label9" runat="server" Text="上课时间(例：周一、三：56节)" CssClass="mdui-textfield-label"></asp:Label>
                <textarea class="mdui-textfield-input" id="add_time" runat="server"></textarea>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-3 mdui-center mdui-text-center">
                <asp:Button ID="Button1" runat="server" Text="添加" OnClick="btn_add" CssClass="mdui-btn mdui-btn-block mdui-shadow-9 mdui-btn-raised mdui-ripple mdui-color-light-blue mdui-text-color-white" />
            </div>
        </div>
        <br />
    </div>
</asp:Content>