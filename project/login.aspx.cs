﻿using System;
using System.Data;
using System.Web;

namespace project
{
    public partial class login : System.Web.UI.Page
    {
        UserManage usermanage = new UserManage();
        MessageManage messagemanage = new MessageManage();
        protected void Page_Load(object sender, EventArgs e)
        {
            string userid = "";
            string userpassword="";
            if (Request.Cookies["cookie_user_id"] != null && Request.Cookies["cookie_user_pwd"] != null)
            {
                userid = Request.Cookies["cookie_user_id"].Value.ToString();
                userpassword = Request.Cookies["cookie_user_pwd"].Value.ToString();
                usermanage.Id = userid;
                usermanage.Password = userpassword;
                DataSet cookies_login = null;
                cookies_login = usermanage.UserLogin(usermanage);
                try { 
                    if (cookies_login.Tables[0].Rows.Count > 0)
                    {
                        Session["userid"] = userid;
                        usermanage.Id = userid;
                        //设置Session过期时间
                        Session.Timeout = 120;
                        DataSet ds = usermanage.GetUserById(usermanage, "用户信息表");
                        //获取用户类型，并删除用户类型中的空格
                        Session["type"] = ds.Tables[0].Rows[0][7].ToString().Replace(" ", "");
                        Response.Redirect("SelfInfo.aspx#nav_info");
                    }
                }
                catch 
                {
                    //清理Cookies,返回
                    Response.Cookies["cookie_user_id"].Expires = System.DateTime.Now.AddSeconds(-1);
                    Response.Cookies["cookie_user_pwd"].Expires = System.DateTime.Now.AddSeconds(-1);
                    Response.Write("<script>alert('系统错误，请重新登录！')</script>");
                }
            }
        }
        protected void BtnLogin_Click(object sender, EventArgs e)
        {
            //用户登录
            DataSet users = null;
            usermanage.Id = loginid.Value;
            usermanage.Password = loginpassword.Value;
            users = usermanage.UserLogin(usermanage);
            //用户登录成功
            if (users.Tables[0].Rows.Count > 0)
            {
                Session["userid"] = loginid.Value;
                usermanage.Id = loginid.Value;
                //设置Session过期时间
                Session.Timeout = 120;
                DataSet ds = usermanage.GetUserById(usermanage, "用户信息表");
                //获取用户类型，并删除用户类型中的空格
                Session["type"]= ds.Tables[0].Rows[0][7].ToString().Replace(" ",""); 
                //设置cookies
                if(auto_login.Checked)
                {
                    Response.Cookies["cookie_user_id"].Value = loginid.Value;
                    Response.Cookies["cookie_user_id"].Expires = DateTime.Now.AddDays(3);
                    Response.Cookies["cookie_user_pwd"].Value=loginpassword.Value;
                    Response.Cookies["cookie_user_pwd"].Expires = DateTime.Now.AddDays(3);
                }
                Response.Redirect("SelfInfo.aspx#nav_info");     
            }
            //用户登录失败
            else
            {
                Response.Write("<script>alert('用户信息不匹配！')</script>");
            }
        }       
    }
}