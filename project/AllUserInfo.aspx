﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AllUserInfo.aspx.cs" Inherits="project.AllUserInfo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>全部用户信息--教务管理系统</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container mdui-cen">
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">全部用户信息查询</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-valign">
            <div class="mdui-center mdui-text-center mdui-shadow-18">
                <asp:GridView ID="GridView1" runat="server" CellPadding="4" DataSourceID="SqlDataSource" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="mdui-table mdui-center" ShowHeaderWhenEmpty="True" EmptyDataText="未查询到用户信息！" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-BorderStyle="NotSet" EmptyDataRowStyle-CssClass="mdui-color-light-blue-50">
                    <AlternatingRowStyle BackColor="#B3E5FC" />
                    <Columns>
                        <asp:BoundField DataField="Id" HeaderText="用户Id" ReadOnly="True" SortExpression="Id" />
                        <asp:BoundField DataField="Password" HeaderText="用户密码" SortExpression="Password" />
                        <asp:BoundField DataField="Name" HeaderText="用户名称" SortExpression="Name" />
                        <asp:BoundField DataField="Age" HeaderText="年&nbsp;龄" SortExpression="Age" />
                        <asp:BoundField DataField="Sex" HeaderText="性&nbsp;别" SortExpression="Sex" />
                        <asp:BoundField DataField="Email" HeaderText="电子邮件" SortExpression="Email" />
                        <asp:BoundField DataField="Tel" HeaderText="联系电话" SortExpression="Tel" />
                        <asp:BoundField DataField="Type" HeaderText="用户类型" SortExpression="Type" />
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#e0f7fa" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <asp:SqlDataSource ID="SqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT * FROM [用户信息表] ORDER BY [Id]"></asp:SqlDataSource>
            </div>
        </div>
        <br />
    </div>
</asp:Content>