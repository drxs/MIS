﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="SentBox.aspx.cs" Inherits="project.SentBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>私信发件箱--教务管理系统</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">私信发件箱</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <asp:LinkButton ID="LinkButton3" runat="server" PostBackUrl="~/SelfInfo.aspx#nav_info" Style="text-decoration: none" CssClass="mdui-btn mdui-ripple" Text="loading" Font-Size="17px"></asp:LinkButton>
                <asp:LinkButton ID="LinkButton1" runat="server" PostBackUrl="~/InquireMessage.aspx#nav_message" Style="text-decoration: none" CssClass="mdui-btn mdui-ripple" Text="进入收件箱" Font-Size="17px"></asp:LinkButton>
                <asp:LinkButton ID="LinkButton2" runat="server" PostBackUrl="~/SendMessage.aspx#nav_message" Style="text-decoration: none" CssClass="mdui-btn mdui-ripple" Text="发送私信" Font-Size="17px"></asp:LinkButton>
            </div>
        </div>
        <br />
        <br />
        <div class="mdui-valign">
            <div class="mdui-center mdui-text-center mdui-shadow-18">
                <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" CssClass="mdui-table mdui-center" EmptyDataText="您未发送过私信！" EmptyDataRowStyle-ForeColor="Red" EmptyDataRowStyle-CssClass="mdui-color-light-blue-50" ShowHeaderWhenEmpty="True">
                    <AlternatingRowStyle BackColor="#B3E5FC"></AlternatingRowStyle>
                    <Columns>
                        <asp:BoundField DataField="私信ID" HeaderText="私信ID" SortExpression="私信ID"></asp:BoundField>
                        <asp:BoundField DataField="接收者" HeaderText="接收者" SortExpression="接收者"></asp:BoundField>
                        <asp:BoundField DataField="发送时间" HeaderText="发送时间" SortExpression="发送时间"></asp:BoundField>
                        <asp:BoundField DataField="消息内容" HeaderText="消息内容" SortExpression="消息内容"></asp:BoundField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#2196f3" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#e0f7fa" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:MISConnectionString %>' SelectCommand="SELECT [私信ID], [接收者], [发送时间], [消息内容] FROM [私信] WHERE ([发送者ID] = @发送者ID) ORDER BY [私信ID] DESC">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="userid" Name="发送者ID" Type="String"></asp:SessionParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <br />
    </div>
</asp:Content>