﻿using System;
using System.Web.UI.WebControls;

namespace project
{
    public partial class SentBox : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //若用户未登录，则跳转至登陆页面
            if (Session["userid"] == null)
            {
                Response.Redirect("login.aspx");
            }
            if (!IsPostBack)
            {
                if (Session["username"] == null)
                {
                    Response.Redirect("login.aspx");
                }
                else
                {
                    //设置顶部工具栏个人信息按钮文字为用户名
                    ((LinkButton)Master.FindControl("self")).Text = Session["username"].ToString();
                    //设置标签文字
                    LinkButton3.Text = "当前用户ID：" + Session["userid"];
                }
            }
        }
    }
}