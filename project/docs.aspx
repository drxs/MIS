﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="docs.aspx.cs" Inherits="project.docs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>说明文档--教务管理系统</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">说明文档</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <div class="mdui-row">
            <div class="mdui-col-xs-12 ">
                <h2 class="mdui-text-color-black">目录</h2>
            </div>
            <div class="mdui-col-xs-4 mdui-center">
                <div class="mdui-typo">
                    <blockquote>
                        <a href="./docs.aspx#introduction_of_system" style='text-decoration: none; color: black'><strong>系统简介</strong></a>
                        <br />
                        <a href="./docs.aspx#introduction_of_function" style='text-decoration: none; color: black'><strong>功能简介</strong></a>
                        <br />
                        <a href="./docs.aspx#cookie_and_session" style='text-decoration: none; color: black'><strong>Cookie与Session</strong></a>
                        <br />
                        <a href="./docs.aspx#authorization" style='text-decoration: none; color: black'><strong>用户角色权限分配</strong></a>
                        <br />
                        <a href="./docs.aspx#update" style='text-decoration: none; color: black'><strong>更新记录</strong></a>
                        <br />
                        <a href="./docs.aspx#reference" style='text-decoration: none; color: black'><strong>代码参考</strong></a>
                        <br />
                        <a href="./docs.aspx#compatibility" style='text-decoration: none; color: black'><strong>网站兼容性测试</strong></a>
                        <br />
                    </blockquote>
                </div>
            </div>
        </div>
        <div class="mdui-row" id="introduction_of_system">
            <div class="mdui-col-xs-12 ">
                <h2 class="mdui-text-color-black">系统简介</h2>
                <p><strong>本系统基于ASP.NET、C#语言、HTML、Microsoft Visual Studio Community 2019、SQL Server on linux 2017完成</strong></p>
            </div>
        </div>
        <div class="mdui-row" id="introduction_of_function">
            <div class="mdui-col-xs-12 mdui-center">
                <h2 class="mdui-text-color-black ">功能简介</h2>
            </div>
            <div class="mdui-col-xs-4 mdui-center">
                <div class="mdui-typo ">
                    <ul>
                        <li><strong>用户信息管理</strong>
                            <ul>
                                <li>个人信息管理</li>
                                <li>添加新用户</li>
                                <li>全部用户信息</li>
                            </ul>
                        </li>
                        <li><strong>课程信息管理</strong>
                            <ul>
                                <li>开课信息查询</li>
                                <li>添加课程信息</li>
                                <li>当前讲授课程</li>
                            </ul>
                        </li>
                        <li><strong>成绩信息管理</strong>
                            <ul>
                                <li>查询成绩信息</li>
                                <li>所有成绩信息</li>
                                <li>添加成绩信息</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="mdui-col-xs-4 mdui-center">
                <div class="mdui-typo">
                    <ul>
                        <li><strong>公告信息管理</strong>
                            <ul>
                                <li>查询公告</li>
                                <li>发布公告</li>
                                <br />
                            </ul>
                        </li>

                        <li><strong>站内信</strong>
                            <ul>
                                <li>收件箱</li>
                                <li>发件箱</li>
                                <li>发送私信</li>
                            </ul>
                        </li>
                        <li><strong>文档</strong>
                            <ul>
                                <li>说明文档</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="mdui-row" id="cookie_and_session">
            <div class="mdui-col-xs-12 ">
                <h2 class="mdui-text-color-black">Cookie与Session</h2>
                <p>
                    <strong>系统使用Cookie与Session提升使用体验，Cookie用于自动登录功能，
                    Session用于系统内不同页面之间的消息传递，详细说明如下：</strong>
                </p>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-center mdui-valign">
            <div class="mdui-col-xs-10 mdui-center">
                <div class="mdui-typo">
                    <div class="mdui-table-fluid  mdui-shadow-18 mdui-center">
                        <table class="mdui-table mdui-table-hoverable">
                            <thead>
                                <tr class="mdui-color-blue-500">
                                    <td><strong>----</strong></td>
                                    <td><strong>cookie_user_id</strong></td>
                                    <td><strong>cookie_user_pwd</strong></td>
                                    <td><strong>ASP.NET_SessionId</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="mdui-color-cyan-50">
                                    <td>用途</td>
                                    <td>保存用户ID，以实现自动登录</td>
                                    <td>保存用户密码，以实现自动登录</td>
                                    <td>保存Session信息</td>
                                </tr>
                                <tr class="mdui-color-light-blue-100">
                                    <td>创建时间</td>
                                    <td>登录账户时</td>
                                    <td>登录账户时</td>
                                    <td>登录账户时</td>
                                </tr>
                                <tr class="mdui-color-cyan-50">
                                    <td>销毁时间</td>
                                    <td>创建后3天或下此自动登录</td>
                                    <td>创建后3天或下次自动登录</td>
                                    <td>浏览器会话断开</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="mdui-row">
            <div class="mdui-col-xs-11 ">
                <p>
                    <strong>登录页面开启自动登录功能后，会自动记录Cookie信息，若在浏览器内清理Cookie，将导致无法自动登录。
                    Session有效时间为2小时，即2小时无任何操作将与服务器断开连接，需重新登录。
                    </strong>
                </p>
            </div>
        </div>
        <div class="mdui-row" id="authorization">
            <div class="mdui-col-xs-12 ">
                <h2 class="mdui-text-color-black">用户角色权限分配</h2>
                <p><strong>系统预设三种用户角色：学生、教师、管理员，根据用户角色不同，赋予不同的访问权限。</strong></p>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-center mdui-valign">
            <div class="mdui-col-xs-10 mdui-center">
                <div class="mdui-table-fluid mdui-shadow-18 mdui-center">
                    <table class="mdui-table mdui-table-hoverable ">
                        <thead>
                            <tr class="mdui-color-blue-500">
                                <td><strong>角色/权限</strong></td>
                                <td><strong>学生</strong></td>
                                <td><strong>教师</strong></td>
                                <td><strong>管理员</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="mdui-color-cyan-50">
                                <td>个人信息管理</td>
                                <td>Yes</td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-light-blue-100">
                                <td>添加新用户</td>
                                <td class="mdui-text-color-red"><strong>No</strong> </td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-cyan-50">
                                <td>全部用户信息</td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-light-blue-100">
                                <td>开课信息查询</td>
                                <td>Yes</td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-cyan-50">
                                <td>添加课程信息</td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-light-blue-100">
                                <td>当前讲授课程</td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                                <td>Yes</td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                            </tr>
                            <tr class="mdui-color-cyan-50">
                                <td>查询成绩信息</td>
                                <td>Yes</td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                            </tr>
                            <tr class="mdui-color-light-blue-100">
                                <td>所有成绩信息</td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-cyan-50">
                                <td>添加成绩信息</td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-light-blue-100">
                                <td>查询公告</td>
                                <td>Yes</td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-cyan-50">
                                <td>发布公告</td>
                                <td class="mdui-text-color-red"><strong>No</strong></td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-light-blue-100">
                                <td>收件箱</td>
                                <td>Yes</td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-cyan-50">
                                <td>发件箱</td>
                                <td>Yes</td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-light-blue-100">
                                <td>发送私信</td>
                                <td>Yes</td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                            <tr class="mdui-color-cyan-50">
                                <td>帮助文档</td>
                                <td>Yes</td>
                                <td>Yes</td>
                                <td>Yes</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <br />
        <div class="mdui-row" id="update">
            <div class="mdui-col-xs-12 ">
                <h2 class="mdui-text-color-black">更新记录</h2>
                <div class="mdui-typo">
                    <blockquote>
                        <strong>2020.05.08</strong><br />
                        1.修复：发送私信功能逻辑错误<br />
                        2.优化：部分文字提示
                        <br />
                        <br />
                        <strong>2020.04.10</strong><br />
                        1.优化：所有表格样式、抽屉导航栏显示效果<br />
                        2.优化：“个人信息”页面显示内容、“私信”页面显示效果、“添加新用户”页面布局<br />
                        3.优化：用户角色权限控制<br />
                        4.优化：部分时间、ID数据由系统自动生成<br />
                        5.新增：3天内自动登录功能<br />
                        6.新增：开启https访问<br />
                        7.修改：部分跳转判断逻辑、跳转提示<br />
                        8.修复：私信功能错误<br />
                        9.修复：System.TypeLoadException错误
                        <br />
                        <br />
                        <strong>2020.03.22</strong><br />
                        1.添加：站内信(私信)功能<br />
                        2.添加：帮助文档<br />
                        3.优化：界面微调<br />
                        4.修复：登录界面背景显示错误问题
                        <br />
                        <br />
                        <strong>2020.02.28</strong><br />
                        1.优化：删除部分代码<br />
                        2.代码：上传至Github<a href="https://github.com/drxs/MIS" target="_blank" style='text-decoration: none; color: black'><strong>『点击跳转』</strong></a>
                        <br />
                        <br />
                        <strong>2020.02.25</strong><br />
                        1.部署：通过Jexus部署至Linux服务器
                        <br />
                        <br />
                        <strong>2020.02.11</strong><br />
                        1.更新：系统主体构建完成
                    </blockquote>
                </div>
            </div>
        </div>
        <div class="mdui-row" id="reference">
            <div class="mdui-col-xs-12 ">
                <h2 class="mdui-text-color-black">代码参考</h2>
                <div class="mdui-typo">
                    <p>
                        1.<a style='color: black'><strong> ASP.NET 4.5网站开发与应用实践教程--清华大学出版社</strong></a>
                        <br />
                        2.<a style='color: black'><strong>ASP.NET项目开发全程实录--清华大学出版社</strong></a>
                        <br />
                        3.<a style='color: black'><strong>C#技术与应用开发--清华大学出版社</strong></a>
                        <br />
                        4.<a href="https://www.mdui.org/docs/" target="_blank" style='text-decoration: none; color: black'><strong>MDUI开发文档</strong></a>
                        <br />
                        5.<a href="https://docs.microsoft.com/zh-cn/sql/linux/sql-server-linux-overview?view=sql-server-linux-2017" target="_blank" style='text-decoration: none; color: black'><strong>Microsoft SQL 文档</strong></a>
                        <br />
                        6.<a href="https://docs.microsoft.com/zh-cn/visualstudio/windows/?view=vs-2019" target="_blank" style='text-decoration: none; color: black'><strong>Visual Studio 文档</strong></a>
                        <br />
                        7.<a href="https://docs.microsoft.com/zh-cn/aspnet/overview" target="_blank" style='text-decoration: none; color: black'><strong>ASP.NET文档</strong></a>
                    </p>
                </div>
            </div>
        </div>
        <div class="mdui-row" id="compatibility">
            <div class="mdui-col-xs-12 ">
                <h2 class="mdui-text-color-black">网站兼容性测试</h2>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-center mdui-valign">
            <div class="mdui-col-xs-10 mdui-center">
                <div class="mdui-typo">
                    <div class="mdui-table-fluid  mdui-shadow-18 mdui-center">
                        <table class="mdui-table mdui-table-hoverable">
                            <thead>
                                <tr class="mdui-color-blue-500">
                                    <td><strong>系统/浏览器</strong></td>
                                    <td><strong>Windows 7</strong></td>
                                    <td><strong>Windows 8.1</strong></td>
                                    <td><strong>Windows 10</strong></td>
                                    <td><strong>Ubuntu(Linux)</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="mdui-color-cyan-50">
                                    <td>Google Chrome</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                </tr>
                                <tr class="mdui-color-light-blue-100">
                                    <td>Firefox火狐浏览器</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                </tr>
                                <tr class="mdui-color-cyan-50">
                                    <td>360安全浏览器</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                    <td>--</td>
                                </tr>
                                <tr class="mdui-color-light-blue-100">
                                    <td>360极速浏览器</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                    <td>兼容性良好</td>
                                    <td>--</td>
                                </tr>
                                <tr class="mdui-color-cyan-50">
                                    <td>Internet Explorer</td>
                                    <td class="mdui-text-color-red"><strong>IE 8.0(不兼容)</strong><br />
                                        <strong>IE 11.0(兼容性较差)</strong></td>
                                    <td class="mdui-text-color-red"><strong>IE 10.0(兼容性较差)</strong><br />
                                        <strong>IE 11.0(兼容性较差)</strong></td>
                                    <td>IE 11.0(兼容性良好)</td>
                                    <td>--</td>
                                </tr>
                                <tr class="mdui-color-light-blue-100">
                                    <td>Microsoft Edge (Chromium)</td>
                                    <td>--</td>
                                    <td>--</td>
                                    <td>兼容性良好</td>
                                    <td>--</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="mdui-row">
            <div class="mdui-col-xs-12 ">
                <p class="mdui-text-color-red"><strong>Internet Explorer浏览器存在兼容性差或不兼容问题，可能会出现控件错位、界面显示不完全的情况，请尽量避免使用！</strong></p>
            </div>
        </div>
        <div class="mdui-row">
            <div class="mdui-col-xs-12 ">
                <p><strong>The document was updated on May 8, 2020.</strong></p>
            </div>
        </div>
    </div>
</asp:Content>

                       