﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="project.AddUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>添加用户--教务管理系统</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">添加新用户</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label7" runat="server" Text="用户类型" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_type" runat="server" CssClass="mdui-select" AutoPostBack="True">
                    <asp:ListItem Selected="True">学生</asp:ListItem>
                    <asp:ListItem>教师</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label1" runat="server" Text="用户ID(请输入数字)" CssClass="mdui-textfield-label"></asp:Label>
                <input class="mdui-textfield-input" id="add_userid" runat="server" required autocomplete="off" />
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label2" runat="server" Text="用户姓名" CssClass="mdui-textfield-label"></asp:Label>
                <input class="mdui-textfield-input" id="add_name" runat="server" required autocomplete="off" />
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label3" runat="server" Text="用户密码" CssClass="mdui-textfield-label"></asp:Label>
                <input class="mdui-textfield-input" id="add_password" runat="server" required />
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label4" runat="server" Text="年龄" CssClass="mdui-textfield-label"></asp:Label>
                <input class="mdui-textfield-input" id="add_age" runat="server" required />
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label5" runat="server" Text="性别" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_sex" runat="server" CssClass="mdui-select">
                    <asp:ListItem Selected="True">男</asp:ListItem>
                    <asp:ListItem>女</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label6" runat="server" Text="电子邮件(可选项)" CssClass="mdui-textfield-label"></asp:Label>
                <input class="mdui-textfield-input" id="add_email" runat="server" />
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" runat="server">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label8" runat="server" Text="所属学院" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="CollegeName" DataValueField="CollegeName" CssClass="mdui-select" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="SqlDataSource1" ConnectionString='<%$ ConnectionStrings:MISConnectionString %>' SelectCommand="SELECT [CollegeName] FROM [学院信息表] ORDER BY [CollegeId]"></asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" runat="server" hidden="hidden">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="label12434" runat="server" Text="学院ID" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_college_id" runat="server" DataSourceID="SqlDataSource2" DataTextField="CollegeId" DataValueField="CollegeId" CssClass="mdui-select" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="SqlDataSource2" ConnectionString='<%$ ConnectionStrings:MISConnectionString %>' SelectCommand="SELECT [CollegeId] FROM [学院信息表] WHERE ([CollegeName] = @CollegeName)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList1" PropertyName="SelectedValue" Name="CollegeName" Type="String"></asp:ControlParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <div class="mdui-row mdui-valign" runat="server" id="label_class">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label10" runat="server" Text="所属班级" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="DropDownList3" runat="server" DataSourceID="SqlDataSource3" DataTextField="ClassName" DataValueField="ClassName" CssClass="mdui-select" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="SqlDataSource3" ConnectionString='<%$ ConnectionStrings:MISConnectionString %>' SelectCommand="SELECT [ClassName] FROM [班级信息表]"></asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" runat="server" hidden="hidden">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label11" runat="server" Text="班级ID" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_class_id" runat="server" DataSourceID="SqlDataSource4" DataTextField="ClassId" DataValueField="ClassId" AutoPostBack="True"></asp:DropDownList>
                <asp:SqlDataSource runat="server" ID="SqlDataSource4" ConnectionString='<%$ ConnectionStrings:MISConnectionString %>' SelectCommand="SELECT [ClassId] FROM [班级信息表] WHERE ([ClassName] = @ClassName)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DropDownList3" PropertyName="SelectedValue" Name="ClassName" Type="String"></asp:ControlParameter>
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <div class="mdui-row mdui-valign" runat="server" id="label_title">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label9" runat="server" Text="教师职称" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="add_title" runat="server" CssClass="mdui-select">
                    <asp:ListItem Selected="True">讲师</asp:ListItem>
                    <asp:ListItem>副教授</asp:ListItem>
                    <asp:ListItem>教授</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign" runat="server">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Button ID="Button1" runat="server" OnClick="AddBtn" Text="添加" CssClass="mdui-btn mdui-btn-block mdui-color-theme mdui-btn-raised" />
            </div>
        </div>
    </div>
</asp:Content>