﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main.Master" AutoEventWireup="true" CodeBehind="AddNotice.aspx.cs" Inherits="project.AddNotice" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>发布公告--教务管理系统</title>
</asp:Content>
<asp:Content ID="MainContent" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="mdui-container ">
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-8 mdui-center mdui-text-center">
                <h1 class="mdui-text-color-theme">发布公告</h1>
            </div>
        </div>
        <div class="mdui-typo">
            <hr />
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label1" runat="server" Text="公告编号(系统自动生成，不可修改)" CssClass="mdui-textfield-label"></asp:Label>
                <asp:TextBox ID="add_id" runat="server" CssClass="mdui-textfield-input"></asp:TextBox>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label2" runat="server" Text="发布时间(当前系统日期)" CssClass="mdui-textfield-label"></asp:Label>
                <asp:TextBox ID="add_time" runat="server" CssClass="mdui-textfield-input" Enabled="false"></asp:TextBox>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label3" runat="server" Text="发布者（当前登录用户）" CssClass="mdui-textfield-label"></asp:Label>
                <asp:DropDownList ID="DropDownList1" runat="server" DataSourceID="SqlDataSource1" DataTextField="Name" DataValueField="Name" CssClass="mdui-select"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:MISConnectionString %>" SelectCommand="SELECT [Name] FROM [用户信息表] WHERE ([Id] = @Id)">
                    <SelectParameters>
                        <asp:SessionParameter Name="Id" SessionField="userid" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-5 mdui-center">
                <asp:Label ID="Label4" runat="server" Text="公告内容(Max：300字符)" CssClass="mdui-textfield-label"></asp:Label>
                <textarea class="mdui-textfield-input" runat="server" id="add_content"></textarea>
            </div>
        </div>
        <br />
        <div class="mdui-row mdui-valign">
            <div class="mdui-col-xs-3 mdui-center mdui-text-center">
                <asp:Button ID="Button1" runat="server" Text="发布" OnClick="Add_NoticeBtn" CssClass="mdui-btn mdui-btn-block mdui-shadow-9 mdui-btn-raised mdui-ripple mdui-color-light-blue mdui-text-color-white" />
            </div>
        </div>
        <br />
    </div>
</asp:Content>
