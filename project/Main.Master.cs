﻿using System;

namespace project
{
    public partial class Main : System.Web.UI.MasterPage
    {
        //设置默认权限
        protected bool IsStudent = false;
        protected bool IsTeacher = false;
        protected bool IsAdmin = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            //根据登录的用户类型设置权限
            if(Session["type"]!=null && Session["type"].ToString()=="学生")
            {
                IsStudent = true;
            }
            if (Session["type"] != null && Session["type"].ToString()=="教师")
            {
                IsTeacher = true;
            }
            if(Session["type"] != null && Session["type"].ToString()=="管理员")
            {
                IsAdmin = true;
            }            
        }
        protected void log_off_btn(object  sender, EventArgs e)
        {
            //设置退出登录按钮点击事件
            try
            {
                Session.Abandon();
                Session.Remove("userid");
                Session.Remove("type");
                Session.Remove("username");
                Response.Cookies.Clear();
                Response.Cookies.Remove("cookie_user_id");
                Response.Cookies.Remove("cookie_user_pwd");
                Response.Cookies["cookie_user_id"].Expires = System.DateTime.Now.AddSeconds(-1);
                Response.Cookies["cookie_user_pwd"].Expires = System.DateTime.Now.AddSeconds(-1);
                if (Session["userid"] == null)
                {
                    //账号退出后，跳转至登录页面
                    Response.Redirect("login.aspx");
                }
            }
            catch
            {
                Response.Write("<script>alert('系统错误！')</script>");
            }
        }
    }
}